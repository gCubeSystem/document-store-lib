package org.gcube.documentstore.configuration;

import java.io.InputStream;
import java.net.URL;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.gcube.common.security.credentials.Credentials;
import org.gcube.common.security.factories.AuthorizationProvider;
import org.gcube.common.security.factories.AuthorizationProviderFactory;
import org.gcube.documentstore.security.provider.DefaultAuthorizationProviderFactory;
import org.ini4j.Ini;
import org.ini4j.Profile.Section;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The configuration of the Document Store Lib
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
public class LibraryConfigurationBinder {

	private static final Logger logger = LoggerFactory.getLogger(LibraryConfigurationBinder.class);
	
	public static final String DOCUMENT_STORE_LIB_CONFIGURATION_FILE = "dsl.ini";
	
	public static final String CONFIGURATION_SECTION_KEY = "configuration";
	public static final String CONFIGURATION_LIFETIME = "configurationExpiringTime";

	public static final String AUTHORIZATION_SECTION_KEY = "authorization";
	public static final String FACTORY_KEY = "factory";
	public static final String FACTORY_KEY_PREFIX = FACTORY_KEY+ ".";
	public static final String CREDENTIALS_KEY = "credentials";
	public static final String CREDENTIALS_KEY_PREFIX = CREDENTIALS_KEY + ".";
	public static final String CREDENTIALS_CLASS_KEY = CREDENTIALS_KEY_PREFIX + "class";

	protected static LibraryConfigurationBinder libraryConfigurationBinder;
	protected static long DEFAULT_LIFETIME = TimeUnit.MINUTES.toMinutes(5);
	

	/**
	 * The time when the library configuration was created.
	 */
	protected Calendar creationTime;

	/**
	 * The time in milliseconds after which the configuration is considered expired.
	 * The default value is 5 minutes (see {@link #DEFAULT_LIFETIME}).
	 * The configuration itself could contains the expiring time.
	 * So that we can decide the configuration lifetime.
	 */
	protected long configurationLifetime;

	protected AuthorizationProvider authorizationProvider;

	private LibraryConfigurationBinder() {
		creationTime = Calendar.getInstance();
		configurationLifetime = DEFAULT_LIFETIME;
	}

	/**
	 * Returns the singleton instance of {@code LibraryConfigurationBinder}.
	 * If the instance does not exist or the configuration has expired, a new instance is created.
	 *
	 * @return the singleton instance of {@code LibraryConfigurationBinder}
	 */
	public static LibraryConfigurationBinder getInstance() {
		Calendar now = Calendar.getInstance();
		Calendar expiringTime = Calendar.getInstance();

		if(libraryConfigurationBinder!=null){
			expiringTime.setTimeInMillis(libraryConfigurationBinder.creationTime.getTimeInMillis());
			expiringTime.add(Calendar.MINUTE, (int)libraryConfigurationBinder.configurationLifetime);
		}

		/**
		 * The very first time libraryConfigurationBinder is instantiated because is null
		 * the second time libraryConfigurationBinder is instantiated only if the configuration has expired
		 */
		if (libraryConfigurationBinder == null || now.after(expiringTime)) {
			libraryConfigurationBinder = new LibraryConfigurationBinder();
		}
		
		return libraryConfigurationBinder;
	}

	public AuthorizationProvider getAuthorizationProvider() throws Exception {
		
		ClassLoader classLoader = LibraryConfigurationBinder.class.getClassLoader();
		URL url = classLoader.getResource(DOCUMENT_STORE_LIB_CONFIGURATION_FILE);
		logger.trace("Going to read {} at {}", DOCUMENT_STORE_LIB_CONFIGURATION_FILE, url.toString());
		InputStream inputStream = classLoader.getResourceAsStream(DOCUMENT_STORE_LIB_CONFIGURATION_FILE);
		
		Ini ini = new Ini(inputStream);

		Section configurationSection = ini.get(CONFIGURATION_SECTION_KEY);
		if(configurationSection != null){
			String expiringTimeString = configurationSection.get(CONFIGURATION_LIFETIME);
			if(expiringTimeString != null){
				try {
					this.configurationLifetime= Long.parseLong(expiringTimeString);
				} catch (NumberFormatException e) {
					logger.warn("ini file error ({}): invalid {} value in {} section. Default value (i.e. {} minutes) wil be used", 
						url.toString(), CONFIGURATION_LIFETIME, CONFIGURATION_SECTION_KEY, DEFAULT_LIFETIME, e);
					
				}
			}

		}else{
			logger.info("ini file error ({}): No {} value in {} section. Default value (i.e. {} minutes) wil be used", 
						url.toString(), CONFIGURATION_LIFETIME, CONFIGURATION_SECTION_KEY, DEFAULT_LIFETIME);
		}
		
		Section authorizationSection = ini.get(AUTHORIZATION_SECTION_KEY);
		if(authorizationSection != null){
			StringBuffer buffer = new StringBuffer();
			buffer.append("ini file (");
			buffer.append(url.toString());
			buffer.append(") error: ");
			

			String provider = authorizationSection.get(FACTORY_KEY);
			AuthorizationProviderFactory<?> authProviderFactory = null;
			if (provider != null) {
				try {
					Object authProviderImpl = Class.forName(provider).getDeclaredConstructor().newInstance();
					authProviderFactory = AuthorizationProviderFactory.class.cast(authProviderImpl);
					authorizationSection.to(authProviderFactory, FACTORY_KEY_PREFIX);
				} catch (Exception e) {
					StringBuffer error = new StringBuffer(buffer);
					error.append(" invalid ");
					error.append(AuthorizationProviderFactory.class.getSimpleName());
					error.append(" type in ");
					error.append(AUTHORIZATION_SECTION_KEY);
					error.append(" section ");
					error.append(FACTORY_KEY);
					error.append(" key.");
					logger.error(error.toString(),e);
					throw new Exception(error.toString(), e);
				}
			} else{
				authProviderFactory = new DefaultAuthorizationProviderFactory();
			}
			
			String credentialClass = authorizationSection.get(CREDENTIALS_CLASS_KEY);
			if (credentialClass == null) {
				StringBuffer error = new StringBuffer(buffer);
				error.append(" not found in ");
				error.append(AUTHORIZATION_SECTION_KEY);
				error.append(" section ");
				error.append(CREDENTIALS_CLASS_KEY);
				error.append(" key.");
				logger.error(error.toString());
				throw new Exception(error.toString());
			}
			
			Credentials credentials;
			try {
				Object credentialsImpl = Class.forName(credentialClass).getDeclaredConstructor().newInstance();
				credentials = Credentials.class.cast(credentialsImpl);
				authorizationSection.to(credentials, CREDENTIALS_KEY_PREFIX);
				authorizationProvider = authProviderFactory.connect(credentials);
			} catch (Exception e) {
				StringBuffer error = new StringBuffer(buffer);
				error.append(" invalid ");
				error.append(Credentials.class.getSimpleName());
				error.append(" type in ");
				error.append(AUTHORIZATION_SECTION_KEY);
				error.append(" section ");
				error.append(CREDENTIALS_CLASS_KEY);
				error.append(" key.");
				logger.error(error.toString(),e);
				throw new Exception(error.toString(), e);
			}

		}

		return authorizationProvider;

	}
	
}
