package org.gcube.documentstore.security.provider;

import java.util.Set;

import org.gcube.common.keycloak.KeycloakClient;
import org.gcube.common.keycloak.KeycloakClientFactory;
import org.gcube.common.security.credentials.Credentials;
import org.gcube.common.security.factories.AuthorizationProvider;
import org.gcube.common.security.secrets.Secret;
import org.gcube.documentstore.security.credentials.DocumentStoreCredential;

public class DefaultAuthorizationProvider implements AuthorizationProvider {

	private KeycloakClient client;
	
	private String endpoint;
	protected DocumentStoreCredential credentials;

	public DefaultAuthorizationProvider(DocumentStoreCredential credentials, String endpoint) {
		this.credentials = credentials;				
		this.endpoint = endpoint;
		this.client = KeycloakClientFactory.newInstance();
	}
	
	@Override
	public Set<String> getContexts() {
		return credentials.getContexts(client, endpoint);
	}

	@Override
	public Secret getSecretForContext(String context) {
		return credentials.getSecretForContext(client, endpoint, context);
	}

	@Deprecated
	@Override
	public Credentials getCredentials() {
		return credentials;
	}
	
}
