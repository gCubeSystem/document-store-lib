package org.gcube.documentstore.security.provider;

import org.gcube.common.security.credentials.Credentials;
import org.gcube.common.security.factories.AuthorizationProviderFactory;
import org.gcube.documentstore.security.credentials.ClientIdCredentials;
import org.gcube.documentstore.security.credentials.DocumentStoreCredential;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class DefaultAuthorizationProviderFactory implements AuthorizationProviderFactory<DefaultAuthorizationProvider>{

	protected String endpoint;

	public DefaultAuthorizationProviderFactory() {
		super();
		this.endpoint = null;
	}
	
	@Override
	public DefaultAuthorizationProvider connect(Credentials credentials) {
		return new DefaultAuthorizationProvider((DocumentStoreCredential) credentials, endpoint);
	}

	@Override
	public String toString() {
		return "DefaultAuthorizationProviderFactory [endpoint=" + endpoint + "]";
	}

	public String getEndpoint() {
		return endpoint;
	}

	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}
	
}
