package org.gcube.documentstore.security.credentials;

import java.util.Set;

import org.gcube.common.keycloak.KeycloakClient;
import org.gcube.common.security.credentials.Credentials;
import org.gcube.common.security.secrets.Secret;

/**
 * The DocumentStoreCredential interface extends the Credentials interface and provides methods 
 * to retrieve contexts and secrets associated with those contexts.
 * 
 */
public interface DocumentStoreCredential extends Credentials {

    public Set<String> getContexts(KeycloakClient client, String endpoint);
    
    public Secret getSecretForContext(KeycloakClient client, String endpoint, String context);

}
