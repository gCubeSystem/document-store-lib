package org.gcube.documentstore.security.credentials;

import java.net.URL;
import java.util.Base64;
import java.util.Set;

import org.gcube.common.keycloak.KeycloakClient;
import org.gcube.common.keycloak.model.TokenResponse;
import org.gcube.common.security.secrets.Secret;
import org.gcube.common.security.secrets.UmaTokenSecret;
import org.gcube.common.validator.annotations.NotEmpty;
import org.gcube.common.validator.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The {@code UserCredentials} class implements the {@code Credentials} interface
 * and represents user credentials with a username and password.
 * 
 * <p>This class ensures that both username and password are not null or empty
 * using the {@code @NotNull} and {@code @NotEmpty} annotations.</p>
 * 
 * <p>It provides getter and setter methods for the username and password fields,
 * and overrides the {@code hashCode}, {@code equals}, and {@code toString} methods
 * from the {@code Object} class.</p>
 * 
 * <p>The {@code equals} method compares the current object with another object
 * to check for equality based on the username and password fields.</p>
 * 
 * <p>The {@code hashCode} method generates a hash code for the object based on
 * the username and password fields.</p>
 * 
 * <p>The {@code toString} method returns a string representation of the object,
 * including the class name and the values of the username and password fields.</p>
 * 
 * <p>Example usage:</p>
 * <pre>
 * {@code
 * UserCredentials credentials = new UserCredentials();
 * credentials.setUsername("user123");
 * credentials.setPassword("pass123");
 * }
 * </pre>
 * 
 * @author Luca
 */
public class UserCredentials implements DocumentStoreCredential {

	private static Logger logger = LoggerFactory.getLogger(UserCredentials.class);

	@NotNull @NotEmpty
	String username;
	
	@NotNull @NotEmpty
	String password;
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String clientID) {
		this.username = clientID;
	}	

	public String getPassword() {
		return password;
	}

	public void setPassword(String secret) {
		this.password = secret;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((username == null) ? 0 : username.hashCode());
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ClientIdCredentials other = (ClientIdCredentials) obj;
		if (username == null) {
			if (other.clientID != null)
				return false;
		} else if (!username.equals(other.clientID))
			return false;
		if (password == null) {
			if (other.secret != null)
				return false;
		} else if (!password.equals(other.secret))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return this.getClass().getSimpleName() + " [username=" + username + ", password=" + password + "]";
	}

	@Override
	public Set<String> getContexts(KeycloakClient client, String endpoint) {
		throw new UnsupportedOperationException("Unimplemented method 'getContexts'");
	}

	protected static String constructBasicAuthenticationHeader(String clientId, String clientSecret) {
        return "Basic " + Base64.getEncoder().encodeToString((clientId + ":" + clientSecret).getBytes());
    }

	@Override
	public Secret getSecretForContext(KeycloakClient client, String endpoint, String context) {
		throw new UnsupportedOperationException("Unimplemented method 'getSecretForContext'");
	}

	
}
