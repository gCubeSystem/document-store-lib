package org.gcube.documentstore.security.credentials;

import java.net.URL;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.gcube.common.keycloak.KeycloakClient;
import org.gcube.common.keycloak.model.AccessToken.Access;
import org.gcube.common.keycloak.model.ModelUtils;
import org.gcube.common.keycloak.model.TokenResponse;
import org.gcube.common.security.ContextBean;
import org.gcube.common.security.secrets.Secret;
import org.gcube.common.security.secrets.UmaTokenSecret;
import org.gcube.common.validator.annotations.NotEmpty;
import org.gcube.common.validator.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The ClientIdCredentials class implements the DocumentStoreCredential interface
 * and provides methods to handle client ID and secret credentials for accessing
 * a document store.
 * 
 * <p>This class includes methods for setting and getting the client ID and secret,
 * as well as methods for obtaining contexts and secrets for specific contexts
 * from a Keycloak client.</p>
 * 
 * <p>Annotations:</p>
 * <ul>
 *   <li>@NotNull: Ensures that the annotated field is not null.</li>
 *   <li>@NotEmpty: Ensures that the annotated field is not empty.</li>
 * </ul>
 * 
 * <p>Fields:</p>
 * <ul>
 *   <li>clientID: The client ID used for authentication.</li>
 *   <li>secret: The secret associated with the client ID.</li>
 * </ul>
 * 
 * <p>Methods:</p>
 * <ul>
 *   <li>getClientID(): Returns the client ID.</li>
 *   <li>setClientID(String clientID): Sets the client ID.</li>
 *   <li>getSecret(): Returns the secret.</li>
 *   <li>setSecret(String secret): Sets the secret.</li>
 *   <li>hashCode(): Generates a hash code for the object.</li>
 *   <li>equals(Object obj): Compares this object with the specified object for equality.</li>
 *   <li>toString(): Returns a string representation of the object.</li>
 *   <li>getContexts(KeycloakClient client, String endpoint): Retrieves the contexts associated with the client ID and secret from the Keycloak client.</li>
 *   <li>getSecretForContext(KeycloakClient client, String endpoint, String context): Retrieves the secret for a specific context from the Keycloak client.</li>
 * </ul>
 * 
 * <p>Logging:</p>
 * <ul>
 *   <li>logger: Logs debug and error messages.</li>
 * </ul>
 */
public class ClientIdCredentials implements DocumentStoreCredential {

	private static Logger logger = LoggerFactory.getLogger(ClientIdCredentials.class);
	
	@NotNull @NotEmpty
	String clientID;
	
	@NotNull @NotEmpty
	String secret;
	
	public String getClientID() {
		return clientID;
	}

	public void setClientID(String clientID) {
		this.clientID = clientID;
	}	

	public String getSecret() {
		return secret;
	}

	public void setSecret(String secret) {
		this.secret = secret;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((clientID == null) ? 0 : clientID.hashCode());
		result = prime * result + ((secret == null) ? 0 : secret.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ClientIdCredentials other = (ClientIdCredentials) obj;
		if (clientID == null) {
			if (other.clientID != null)
				return false;
		} else if (!clientID.equals(other.clientID))
			return false;
		if (secret == null) {
			if (other.secret != null)
				return false;
		} else if (!secret.equals(other.secret))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return this.getClass().getSimpleName() + " [clientID=" + clientID + ", secret=" + secret + "]";
	}

	@Override
	public Set<String> getContexts(KeycloakClient client, String endpoint) {
		Set<String> contexts = new HashSet<String>();
		try {
			TokenResponse response = null;
			if(endpoint == null || endpoint.isEmpty()) {
				response = client.queryOIDCToken(this.getClientID(), this.getSecret());
			}else{
				response = client.queryOIDCToken(new URL(endpoint), this.getClientID(), this.getSecret());
			}
			Map<String, Access> resourceAccess = ModelUtils.getAccessTokenFrom(response).getResourceAccess();
			for (String context : resourceAccess.keySet()) {
				try {
					ContextBean scope = new ContextBean(context.replaceAll("%2F", "/"));
					contexts.add(scope.toString());
					logger.debug("found context {}",context);
				}catch (IllegalArgumentException e) {
					logger.debug("invalid context found in token: {}", context);
				}
			}
		} catch (Exception e) {
			logger.error("error getting OIDToken from keycloak",e);
			return Collections.emptySet();
		}
		return contexts;
	}

	@Override
	public Secret getSecretForContext(KeycloakClient client, String endpoint, String context) {
		try {
			TokenResponse response = null;
			if(endpoint == null || endpoint.isEmpty()) {
				response = client.queryUMAToken(this.getClientID(), this.getSecret(), context, null);
			}else{
				response = client.queryUMAToken(new URL(endpoint), this.getClientID(), this.getSecret(), context, null);
			}
			return new UmaTokenSecret(response.getAccessToken());
		} catch (Exception e) {
			logger.error("error getting OIDToken from keycloak",e);
			throw new RuntimeException("error getting access token for context "+context, e);
		}
	}
	
}
