package org.gcube.documentstore.configuration;

import org.gcube.common.security.factories.AuthorizationProvider;
import org.gcube.common.security.providers.SecretManagerProvider;
import org.gcube.common.security.secrets.Secret;
import org.gcube.informationsystem.resourceregistry.ContextTest;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The configuration of the Document Store Lib
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
public class LibraryConfigurationBinderTest extends ContextTest{

	private static final Logger logger = LoggerFactory.getLogger(LibraryConfigurationBinderTest.class);
	
	@Test
	public void test() throws Exception {
		LibraryConfigurationBinder lcb = LibraryConfigurationBinder.getInstance();
		AuthorizationProvider authorizationProvider = lcb.getAuthorizationProvider();
		Secret secret = authorizationProvider.getSecretForContext(SecretManagerProvider.get().getContext());
		logger.info("Id {} - Context {}", secret.getOwner().getId(),  secret.getContext());
	}
	
}
