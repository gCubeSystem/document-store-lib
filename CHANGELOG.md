This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for Document Store Library

## [v4.0.0-SNAPSHOT]

- Migrated code to comply with smartgears 4
- Moved to Java 11
- Upgraded maven-parent to 1.2.0
- Upgraded gcube-bom to 4.0.0


## [v3.0.1]

- Fixed log which causes null pointer exception in case of null value provided


## [v3.0.0]

- Switched JSON management to gcube-jackson [#19115]
- Changed the way to manage scheduled thread termination [#18547]


## [v2.5.0] [r4.15.0] - 2019-11-11

- Fixed distro files and pom according to new release procedure


## [v2.4.0] [r4.12.1] - 2018-10-10

- Fixed bug causing too many interaction with IS
- Assigned a distinguish name to different threads


## [v2.3.0] [r4.11.0] - 2018-04-12

- Deprecated unneeded method


## [v2.2.0] [r4.10.0] - 2017-02-15

- Fixed bug which causes duplication of records in fallback files when forcing use of fallback [#10677]
- Changed pom.xml to use new make-servicearchive directive [#10146]
- Added possibility to marshal/unmarshall list and array of Records [#10804]


## [v2.1.0] [r4.7.0] - 2017-10-09

- Added abstract method isConnectionActive() in PersistenceBackend
- Migrated document-store marshalling/unmarshalling to Jackson [#9035]


## [v2.0.0] - 2017-06-07

- Added Jackson support on Usage Record model to allow to use it for marshalling and unmarshalling


## [v1.5.1] [r4.4.0] - 2017-05-02

- Added shutdown() method [#7345]


## [v1.5.0] [r4.3.0] - 2017-03-16

- Renewing persistence configuration after repeated failures [#6277]
- Added new api to close connection to persistence if any


## [v1.4.0] [r4.2.0] - 2016-12-15

- Added support to allow client to set no aggregation [#5808]


## [v1.3.0] [r4.1.0] - 2016-11-07

- Repetitive timeout are managed appropriately
- Added JobUsageRecord aggregation by providing AggregatedJobUsageRecord class


## [v1.2.0] [r4.0.0] - 2016-07-28

- Added a configuration to set aggregation parameters (defaults are provided) [#4240]
- Aggregation parameters are load from a property file or from a Service Endpoint (property file has priority over ServiceEndpoint)


## [v1.1.0] [r3.11.0] - 2016-05-18

- Removed dependency over reflection library [#2357]


## [v1.0.1] [r3.10.1] - 2016-04-08

- Fixed distro directory


## [v1.0.0] [r3.10.0] - 2016-02-08

- First Release

